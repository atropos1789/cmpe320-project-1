! Problem 3.2
! For N in 100, 1000, 1000
! For p1 in 0.5, 0.2, 0.7
! 1. Generate N 100-character binary strings with probability of 1 = p1
! 2. Determine the first 1 in each string
! 3. Determine the mean and standard deviation of the location of the first 1

! turn program into a function that accepts N and p1 as arguments

PROGRAM problem_3_2
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none
    integer :: n(3)
    real    :: p1(3)
    integer :: i, j, k
!    integer, allocatable :: data_binned(:)
    integer, allocatable :: seed(:)

    n = [ 100, 1000, 10000 ]
    p1 = [ 0.5, 0.7, 0.2 ]
    
    call random_seed(size = k)
    allocate(seed(k))
    call random_seed(get=seed) 
    
    do i=1, size(n)
        do j=1,size(p1)
            call do_problem_3_2( n(i), p1(j) )
        enddo
    enddo
    

! figure out how to bin data, using a dynamically allocated array based on the largest number of the data array
!    do i=1,n(1)
!        data_binned(randomInt) = data_binned(randomInt) + 1
!    enddo
!    data_binned = data_binned / real()
 
! once this works for fixed n and p1, run it for all 9 combinations of them
!    do i=i,3
!        do j=1,3
!           problem_3_2(n(i), p1(j))
!        enddo
!    enddo
    
CONTAINS

    SUBROUTINE do_problem_3_2(n, p1)
        integer, intent(in) :: n
        real, intent(in) :: p1
        integer :: i, j
        real :: randomReal
        integer :: raw_data(n,100) !each row is a different string
        real(dp) :: data(n)
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance

        ! empty variables
        raw_data(:,:) = 0
        data(:) = 0
        data_mean = 0
        pop_mean = 0
        data_variance = 0
        pop_variance = 0

        ! print values of n, p1
        print *, 'n equals', n
        print *, 'p1 equals', p1
        
        ! create raw data
        do i=1,n
            do j=1,100
                call random_number(randomReal)
                if (randomReal >= p1) then 
                    raw_data(i,j) = 0
                else
                    raw_data(i,j) = 1
                endif
            enddo
        enddo

        ! create data
        ! find first 1 in each row of the array
        row: do i=1,n
            col: do j=1,100
                if ( raw_data(i,j) == 1 ) then
                    data(i) = j
                    exit col ! exit the do loop once the first 1 is found
                endif
            enddo col
        enddo row
        
        ! compute data mean
        do i=1,n
            data_mean = data_mean + (data(i) / real(n) )
        enddo

        ! compute data variance 
        do i=1,n
            data_variance = data_variance + (1.0/ real(n) )*(data_mean - data(i))**2
        enddo

        ! compute population mean and variance
        pop_mean = 1 / p1
        pop_variance = ( 1 - p1 ) / ( p1**2 )  

        ! print values to console  
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'population mean', pop_mean
        print *, 'population variance', pop_variance
        
    END SUBROUTINE do_problem_3_2

END PROGRAM problem_3_2
