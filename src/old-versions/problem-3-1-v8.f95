! Problem 3.1
!    for N in [ 240 2400 24000 240000 ]
!    1. Choose a random integer \in [1,8] N times
!    2. put all of the random trials into an array
!    3. compute the sample mean and sample variance of the array
!    4. plot a histogram of the array, and a stem plot of the theoretical values

! re-add graphing 

PROGRAM problem_3_1
    use plplot
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc
    integer, parameter :: n(4) = [ 240, 2400, 24000, 240000 ]
    integer :: i

    ! graph preparation
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    
    call plgdev(driver)
    call plspal0('cmap0_black_on_white.pal')
    call plgfam(fam,num,bmax)
    call plinit()

    ! run the experiment for all values of n
    do i = 1, size(n)
        call do_problem_3_1(n(i))
    enddo

CONTAINS

    SUBROUTINE do_problem_3_1(n)
    
        integer, intent(in) :: n 
        integer :: i
        real :: randomReal
        integer :: maxValue
        integer :: data(n)
        real(dp), allocatable :: binned_data(:), bin_vals(:)
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance
        integer :: lowerbound, upperbound
        integer :: maxval, minval, range
        real(dp) :: maxvalReal, minvalReal
        real(dp) :: xmin, xmax, ymin, ymax
        real(dp) :: true_value(8)
        character(len=30) :: plot_label
        character(len=20) :: format_string

        ! initialize variables 
        data(:) = 0
        data_mean = 0.0
        pop_mean = 0.0
        data_variance = 0.0
        pop_variance = 0.0
        lowerbound = 1
        upperbound = 8
        
        ! create data
        data = random(lowerbound, upperbound, n)

        ! find data bounds and range 
        do i = 1, size(data)
            if ( data(i) >= maxvalReal ) maxvalReal = data(i)
            if ( data(i) <= minvalReal ) minvalReal = data(i)
        enddo
        maxval = int(maxvalReal)
        minval = int(minvalReal)
        range = abs(minval) + abs(maxval)

        ! bin and normalize data
        allocate(binned_data(range))
        allocate(bin_vals(range))
        binned_data(:) = 0
        bin_vals = [(i + minval, i = 1, range)]
        do i = 1, n
            binned_data(data(i)) = binned_data(data(i)) + 1
        enddo
        binned_data = binned_data / real(n)

        ! compute data mean and variance
        data_mean = sum(data)/real(n)
        data_variance = sum((data_mean - data)**2) / real(n)

        ! compute population mean and variance
        pop_mean = 4.5_dp
        pop_variance = 5.25_dp

        ! print values to console
        print *, '---------------------------'
        print *, 'n equals', n
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'population mean:', pop_mean
        print *, 'population variance:', pop_variance

        ! graphing code
        xmin = 0.5_dp
        xmax = 8.5_dp
        ymin = 0.0_dp
        ymax = 0.2_dp
        true_value(:) = 0.125_dp
        if (n < 10) then
            format_string = "(A22,I1)"
        else if (n < 100) then
            format_string = "(A22,I2)"
        else if (n < 1000) then
            format_string = "(A22,I3)"
        else if (n < 10000) then
            format_string = "(A22,I4)"
        else if (n < 100000) then
            format_string = "(A22,I5)"
        else if (n < 1000000) then
            format_string = "(A22,I6)"
        endif
        write (plot_label ,format_string) 'Section 3.1, Ntrials: ', n
        call plenv(xmin, xmax, ymin, ymax, 0, 0)
        call pllab('Dice Side', 'Probability', trim(plot_label))
        
        !> graph data
        call plbin(bin_vals, binned_data, 1)
        
        !> graph analytic values
        call pljoin(1.0_dp, 0.0_dp, 1.0_dp, 0.125_dp)
        call pljoin(2.0_dp, 0.0_dp, 2.0_dp, 0.125_dp)
        call pljoin(3.0_dp, 0.0_dp, 3.0_dp, 0.125_dp)
        call pljoin(4.0_dp, 0.0_dp, 4.0_dp, 0.125_dp)
        call pljoin(5.0_dp, 0.0_dp, 5.0_dp, 0.125_dp)
        call pljoin(6.0_dp, 0.0_dp, 6.0_dp, 0.125_dp)
        call pljoin(7.0_dp, 0.0_dp, 7.0_dp, 0.125_dp)
        call pljoin(8.0_dp, 0.0_dp, 8.0_dp, 0.125_dp)
        call plstring(bin_vals, true_value, '•')

        
        call pladv(1)

    END SUBROUTINE do_problem_3_1

END PROGRAM problem_3_1
