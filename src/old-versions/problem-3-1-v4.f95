! Problem 3.1
!    for N in [ 240 2400 24000 240000 ]
!    1. Choose a random integer \in [1,8] N times
!    2. put all of the random trials into an array
!    3. compute the sample mean and sample variance of the array
!    4. plot a histogram of the array, and a stem plot of the theoretical values

! add stem plots

PROGRAM project_1
    use plplot
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double

    implicit none 
    
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc
    integer, allocatable :: seed(:)
    integer              :: n
    
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    
    call random_seed(size = n)
    allocate(seed(n))
    call random_seed(get=seed)   

    call plgdev(driver)
    call plgfam(fam,num,bmax)
    call plinit()
    call plcol0(6)
    
    call solveproblem_3_1(240)
    call solveproblem_3_1(2400)
    call solveproblem_3_1(24000)
    call solveproblem_3_1(240000)

    call plend

CONTAINS

    SUBROUTINE solveproblem_3_1(n)
        integer, intent(in) :: n 
        integer :: i, maxValue, randomInt
        real :: randomReal
        real(dp) :: data(n)
!        real(dp), allocatable :: data_binned(:)
        real(dp) :: data_binned(8)
        real(dp) :: bin_value(8) 
        real(dp) :: xmin, xmax, ymin, ymax
        real(dp) :: true_value(8)

        xmin = 0.5_dp
        xmax = 8.5_dp
        ymin = 0.0_dp
        ymax = 0.2_dp

        true_value(:) = 0.125_dp


        maxValue = 8
!        allocate(data_binned(maxValue))
        
        data(:) = 0
        data_binned(:) = 0

        bin_value = [1.0_dp, 2.0_dp, 3.0_dp, 4.0_dp, 5.0_dp, 6.0_dp, 7.0_dp, 8.0_dp ]

        do i=1,n
            call random_number(randomReal)
            randomInt = floor(randomReal * maxValue) + 1
            data(i) = randomInt 
            data_binned(randomInt) = data_binned(randomInt) + 1
        enddo

        data_binned = data_binned / real(n)
        call plenv(xmin, xmax, ymin, ymax, 0, 0)
        call plbin(bin_value, data_binned, 1)
        call pljoin(1.0_dp, 0.0_dp, 1.0_dp, 0.125_dp)
        call pljoin(2.0_dp, 0.0_dp, 2.0_dp, 0.125_dp)
        call pljoin(3.0_dp, 0.0_dp, 3.0_dp, 0.125_dp)
        call pljoin(4.0_dp, 0.0_dp, 4.0_dp, 0.125_dp)
        call pljoin(5.0_dp, 0.0_dp, 5.0_dp, 0.125_dp)
        call pljoin(6.0_dp, 0.0_dp, 6.0_dp, 0.125_dp)
        call pljoin(7.0_dp, 0.0_dp, 7.0_dp, 0.125_dp)
        call pljoin(8.0_dp, 0.0_dp, 8.0_dp, 0.125_dp)

        call plstring(bin_value, true_value, '•')
        call pladv(1)

    END SUBROUTINE solveproblem_3_1

END PROGRAM project_1
