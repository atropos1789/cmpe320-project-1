! Problem 3.3
! For N in 100, 1000, 100000
!   run a *symmetric* lambda sample (also called Laplace distribution)

! Inital attempt 

PROGRAM problem_3_3
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none

    integer         :: n(3)
    real(dp)        :: lambda
    integer         :: i

    n = [ 100, 1000, 100000 ]
    lambda = 0.7
    
    ! run the experiment for all values of n
    do i = 1, size(n)
        call do_problem_3_3( n(i), lambda )
    enddo
    
CONTAINS

    SUBROUTINE do_problem_3_3(n, lambda)

        integer, intent(in) :: n
        real(dp), intent(in) :: lambda
        integer :: i, j
        integer :: maxval
        integer :: minval
        real(dp) :: maxvalReal
        real(dp) :: minvalReal
        real(dp) :: data(n)
        integer, allocatable :: binned_data(:), bin_vals(:)
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance
        integer :: range

        ! empty variables
        data(:) = 0.0_dp
        data_mean = 0.0_dp
        pop_mean = 0.0_dp
        data_variance = 0.0_dp
        pop_variance = 0.0_dp
        maxval = 0
        minval = 0
        maxvalReal = 0.0_dp
        minvalReal = 0.0_dp
        
        data = exponential_dist(n, lambda)

        ! make data spread over -inf to +inf
        do i = 1, n/2
            data(i) = -1.0_dp * data(i)
        enddo

        ! NOTE: prof tells us to use a lambda distribution scaled by 0.5
        ! I need to check if that is happening 

        ! find data bounds and range 
        do i = 1, size(data)
            if ( data(i) >= maxvalReal ) maxvalReal = data(i)
            if ( data(i) <= minvalReal ) minvalReal = data(i)
        enddo
        maxval = int(maxvalReal)
        minval = int(minvalReal)
        range = abs(minval) + abs(maxval)

        ! bin data
        ! TODO
        allocate(binned_data(range))
        allocate(bin_vals(range))
        binned_data(:) = 0
        bin_vals = [(i + minval, i = 1, range)]


        ! NOTE: ask prof.
        ! should the bins be centered on integer values of x or can I have them centered on whatever value of x is convenient (namely, x=0.5, x=1.5,...)
!        do i = 1, size(data)
!            j = int(data(i))
!            binned_data(j) = binned_data(j) + 1
!        enddo

        
        ! compute data mean and variance
        data_mean = sum(data)/real(n)
        data_variance = sum((data_mean - data)**2) / real(n)
    
        ! compute population mean and variance
        pop_mean = 0
        pop_variance = 2.0 / lambda**2 

        ! print values to console  
        print *, '---------------------------'
        print *, 'n equals', n
        print *, 'lambda equals', lambda
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'population mean:', pop_mean
        print *, 'population variance:', pop_variance
!        print *, 'largest value', maxvalReal
!        print *, 'smallest value', minvalReal
!        print *, 'largest value (int)', maxval
!        print *, 'smallest value (int)', minval
!        print *, bin_vals
        
        ! graph data
        ! remember to plot the anayltical pdf 
        
    END SUBROUTINE do_problem_3_3

END PROGRAM problem_3_3
