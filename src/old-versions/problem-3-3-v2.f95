PROGRAM problem_3_3
    use plplot
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

    !> General Variables
    integer :: i, j, k
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc

    !> Problem Parameters
    integer, parameter :: n = [ 100, 1000, 100000 ] 
    real(dp)        :: lambda = 0.7_dp

    ! graph preparation
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    call plgdev(driver)
    call plspal0('cmap0_black_on_white.pal')
    call plgfam(fam,num,bmax)
    call plinit()

    !> Do Problem
    call do_problem_3_3( n, lambda )


CONTAINS

    SUBROUTINE do_problem_3_3(n, lambda)

        ! declare variables
        integer, intent(in) :: n
        real(dp), intent(in) :: lambda
        integer :: i, j
        integer :: maxval, minval, range, num_bins, rounded_size
        real(dp) :: maxvalReal, minvalReal, bin_size
        real(dp) :: data(n), index
        real(dp), allocatable :: binned_data(:) 
        real(dp), allocatable :: bin_vals(:)
        real(dp), allocatable :: analytic_data(:)
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance
        real(dp) :: xmin, xmax, ymin, ymax
        character(len=30) :: plot_label
        character(len=20) :: format_string

        ! initialize variables
        data(:) = 0.0_dp
        data_mean = 0.0_dp
        pop_mean = 0.0_dp
        data_variance = 0.0_dp
        pop_variance = 0.0_dp
        minval = 0
        maxval = 0
        minvalReal = 0.0_dp
        maxvalReal = 0.0_dp
        bin_size = 0.5_dp

        ! create raw data 
        data = exponential_dist(n, lambda)

        ! create data (make half of the values negative)
        do i = 1, n/2
            data(i) = -1.0_dp * data(i)
        enddo

        ! find data bounds and range 
        do i = 1, size(data)
            if ( data(i) <= minvalReal ) minvalReal = data(i)
            if ( data(i) >= maxvalReal ) maxvalReal = data(i)
        enddo
        minval = int(minvalReal)
        maxval = int(maxvalReal)
        range = abs(minval) + abs(maxval)
        num_bins = int(real(range, kind=dp) / bin_size)

        ! bin data
!        allocate(binned_data(num_bins), source=0.0_dp)
        allocate(bin_vals(num_bins), source=0.0_dp)
        
        bin_vals = [( bin_size * real(i, kind=dp) + real(minval, kind=dp), i = 1, num_bins )]
                
!        call bin_data(data, num_bins, minval, maxval, range, binned_data)
!        do j = 1, size(data)
!            rounded_size = int(data(j) - minvalReal) 
!            if (rounded_size <= size(binned_data)) binned_data(rounded_size) = binned_data(rounded_size) + 1.0
!        enddo


        ! compute sample mean and variance
        data_mean = sum(data)/real(n, kind=dp)
        data_variance = sum((data_mean - data)**2) / real(n, kind=dp)

        ! compute population mean and variance
        pop_mean = 0
        pop_variance = 2.0 / lambda**2 

        ! construct analytic values
        allocate(analytic_data(num_bins))
        
        do i = 1, size(bin_vals)
            analytic_data(i) = 0.5_dp * lambda * exp( -1.0_dp * lambda * abs( bin_vals(i) ) )
        enddo

        ! print values to console
        print *, '---------------------------'
        print *, 'n equals', n
        print *, 'lambda equals', lambda
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'absolute error in mean', abs(pop_mean - data_mean)
        print *, 'absolute error in variance', abs(pop_variance - data_variance)
        print *, 'population mean:', pop_mean
        print *, 'population variance:', pop_variance
        print *, 'bin vals', bin_vals
        print *, 'analytic data', analytic_data

        ! setup graph
        xmin = real(minval, kind=dp) + (bin_size / 2.0_dp)
        xmax = real(maxval, kind=dp) + (bin_size / 2.0_dp)
        ymin = 0.0_dp
        ymax = 0.4_dp

        if (n < 10) then
            format_string = "(A22,I1)"
        else if (n < 100) then
            format_string = "(A22,I2)"
        else if (n < 1000) then
            format_string = "(A22,I3)"
        else if (n < 10000) then
            format_string = "(A22,I4)"
        else if (n < 100000) then
            format_string = "(A22,I5)"
        else if (n < 1000000) then
            format_string = "(A22,I6)"
        endif

        write (plot_label, format_string) 'Section 3.3, Ntrials: ', n
        call plenv(xmin, xmax, ymin, ymax, 0, 0)
        call pllab('', 'Probability', trim(plot_label))


        ! ! graph data
!        call plbin(bin_vals, binned_data, 1)
        call plbin(bin_vals, analytic_data, 1)

        ! ! graph analytic data
        call plstring(bin_vals, analytic_data, '•')
        do i = 1, num_bins
            index = real(i, kind=dp) * bin_size + real(minval, kind=dp)
            call pljoin( index, 0.0_dp, index, analytic_data(i))
        enddo     

        
        !! Finish up
        !call pladv(1)
        call pleop()
        call plbop()

    END SUBROUTINE do_problem_3_3

END PROGRAM problem_3_3
