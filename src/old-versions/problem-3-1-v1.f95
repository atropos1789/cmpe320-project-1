! Problem 3.1
!    for N in [ 240 2400 24000 240000 ]
!    1. Choose a random integer \in [1,8] N times
!    2. put all of the random trials into an array
!    3. compute the sample mean and sample variance of the array
!    4. plot a histogram of the array, and a stem plot of the theoretical values

!!!! V1

PROGRAM project_1
    use plplot
    implicit none 
    
    character(len=80) :: driver
    real(kind=pl_test_flt)     :: experiment_1_data(240)
    real(kind=pl_test_flt)     :: experiment_2_data(2400)
    real(kind=pl_test_flt)     :: experiment_3_data(24000)

    integer, allocatable :: seed(:)
    integer              :: n, i, maxValue, randomInt
    real                 :: randomReal
    integer :: fam, num, bmax
    integer :: plparseopts_rc, plsetopt_rc

    
    experiment_1_data(:) = 0
    experiment_2_data(:) = 0
    experiment_3_data(:) = 0
    maxValue = 8

    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"


    call random_seed(size = n)
    allocate(seed(n))
    call random_seed(get=seed)   

    do i=1,240
        call random_number(randomReal)
        randomInt = floor(randomReal * maxValue) + 1
        experiment_1_data(i) = randomInt 
    enddo
         

    call plgdev(driver)
    call plgfam(fam,num,bmax)
    call plinit()
    call plcol0(6)
    call plhist(experiment_1_data(:), 0.5_pl_test_flt, 8.5_pl_test_flt, 8, 0)

    call pladv(1)
    
    do i=1,2400
        call random_number(randomReal)
        randomInt = floor(randomReal * maxValue) + 1
        experiment_2_data(i) = randomInt 
    enddo
    
    call plhist(experiment_2_data(:), 0.5_pl_test_flt, 8.5_pl_test_flt, 8, 0)
    call pladv(1)

    do i=1,24000
        call random_number(randomReal)
        randomInt = floor(randomReal * maxValue) + 1
        experiment_3_data(i) = randomInt 
    enddo

    call plhist(experiment_3_data(:), 0.5_pl_test_flt, 8.5_pl_test_flt, 8, 0)

    call plend

END PROGRAM project_1
