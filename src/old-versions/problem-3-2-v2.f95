! Problem 3.2
! For N in 100, 1000, 1000
! For p1 in 0.5, 0.2, 0.7
! 1. Generate N 100-character binary strings with probability of 1 = p1
! 2. Determine the first 1 in each string
! 3. Determine the mean and standard deviation of the location of the first 1

! Fix nested loops, compute mean and variance for the sample and the population

PROGRAM problem_3_2
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none
!    integer :: n(3)
!    real    :: p1(3)
    integer :: n
    real    :: p1
    integer :: i, j, k
    integer :: raw_data(100,100) !each row is a different string
    integer :: data(100)
!    integer, allocatable :: data_binned(:)
    real(dp) :: data_mean, pop_mean
    real(dp) :: data_variance, pop_variance
    integer, allocatable :: seed(:)
    real :: randomReal
    integer :: randomInt

    n = 100
    p1 = 0.5
    
    call random_seed(size = k)
    allocate(seed(k))
    call random_seed(get=seed) 
    
    do i=1,100
        do j=1,n
            call random_number(randomReal)
            if (randomReal >= p1) then 
                randomInt = 0
            else
                randomInt = 1
            endif
            raw_data(i,j) = randomInt 
        enddo
    enddo

    ! FIND FIRST 1
    row: do i=1,100
        col: do j=1,n
        ! in each row, determine the first 1 by scanning
            if ( raw_data(i,j) == 1 ) then
                data(i) = j
                exit col
            endif
        enddo col
    enddo row
    
    ! compute data mean
    do i=1,100
        data_mean = data_mean + (data(i) / 100.0)
    enddo

    ! compute data variance 
    do i=1,100
        data_variance = data_variance + (1.0/100.0)*(data_mean - data(i))**2
    enddo

    ! compute population mean and variance
    pop_mean = 1 / p1
    pop_variance = ( 1 - p1 ) / ( p1**2 )    

    print *, 'sample mean: ', data_mean
    print *, 'sample variance: ', data_variance
    print *, 'population mean', pop_mean
    print *, 'population variance', pop_variance


! figure out how to bin data, using a dynamically allocated array based on the largest number of the data array
!    do i=1,n(1)
!        data_binned(randomInt) = data_binned(randomInt) + 1
!    enddo
!    data_binned = data_binned / real()
 
! once this works for fixed n and p1, run it for all 9 combinations of them
!    do i=i,3
!        do j=1,3
!           problem_3_2(n(i), p1(j))
!        enddo
!    enddo
    
CONTAINS


END PROGRAM problem_3_2
