! Problem 3.1
!    for N in [ 240 2400 24000 240000 ]
!    1. Choose a random integer \in [1,8] N times
!    2. put all of the random trials into an array
!    3. compute the sample mean and sample variance of the array
!    4. plot a histogram of the array, and a stem plot of the theoretical values

! refactor, calculate sample mean and variance (without graphing)

PROGRAM project_1
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 
    
    integer              :: n(4)
    integer              :: i, k
    integer, allocatable :: seed(:)

    n = [ 240, 2400, 24000, 240000 ]
    
    ! initialize random seed    
    call random_seed(size = k)
    allocate(seed(k))
    call random_seed(get=seed)   

    ! run the experiment for all values of n
    do i = 1, size(n)
        call do_problem_3_1(n(i))
    enddo

CONTAINS

    SUBROUTINE do_problem_3_1(n)
        integer, intent(in) :: n 
        integer :: i
        real :: randomReal
        integer :: maxValue
        integer :: data(n)
        real(dp), allocatable :: binned_data(:)
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance

        ! empty variables 
        data(:) = 0
        data_mean = 0
        pop_mean = 0
        data_variance = 0
        pop_variance = 0
        
        ! print value of n
        print *, '---------------------------'
        print *, 'n equals', n

        ! create data
        do i = 1, n
            call random_number(randomReal)
            data(i) =  floor(randomReal * 8) + 1
        enddo

        ! find largest value in data
        maxValue = ubound(data, 1)

        ! create array to hold binned data
        allocate(binned_data(maxValue))
        binned_data(:) = 0
        
        ! bin data
        do i = 1, n
            binned_data(data(i)) = binned_data(data(i)) + 1
        enddo

        ! compute data mean
        data_mean = sum(data)/real(n)

        ! compute data variance 
        data_variance = sum((data_mean - data)**2) / real(n)

        ! compute population mean and variance
        pop_mean = 4.5
        pop_variance = 5.25 

        ! print values to console
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'population mean', pop_mean
        print *, 'population variance', pop_variance

        ! graph data
        
    END SUBROUTINE do_problem_3_1

END PROGRAM project_1
