! Problem 3.1
!    for N in [ 240 2400 24000 240000 ]
!    1. Choose a random integer \in [1,8] N times
!    2. put all of the random trials into an array
!    3. compute the sample mean and sample variance of the array
!    4. plot a histogram of the array, and a stem plot of the theoretical values

! add labels to graph 

PROGRAM project_1
    use plplot
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double

    implicit none 
    
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc
    integer, allocatable :: seed(:)
    integer              :: n
    integer :: i
    integer :: r(16), g(16), b(16)
    
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    
    call random_seed(size = n)
    allocate(seed(n))
    call random_seed(get=seed)   

    call plgdev(driver)
    
    call plspal0('cmap0_black_on_white.pal')
!    call plspal0('project1.pal')

    call plgfam(fam,num,bmax)
    call plinit()
    
    call solveproblem_3_1(240)
    call solveproblem_3_1(2400)
    call solveproblem_3_1(24000)
    call solveproblem_3_1(240000)

!    do i=1,16
!        call plgcol0(i, r(i), g(i), b(i))
!    enddo
!    print *, r, g, b

    call plend

    

CONTAINS

    SUBROUTINE solveproblem_3_1(n)
        integer, intent(in) :: n 
        integer :: i, maxValue, randomInt
        real :: randomReal
        real(dp) :: data(n)
!        real(dp), allocatable :: data_binned(:)
        real(dp) :: data_binned(8)
        real(dp) :: bin_value(8) 
        real(dp) :: xmin, xmax, ymin, ymax
        real(dp) :: true_value(8)
        character(len=30) :: plot_label
        character(len=20) :: format_string

        xmin = 0.5_dp
        xmax = 8.5_dp
        ymin = 0.0_dp
        ymax = 0.2_dp

        true_value(:) = 0.125_dp

        if (n < 10) then
            format_string = "(A22,I1)"
        else if (n < 100) then
            format_string = "(A22,I2)"
        else if (n < 1000) then
            format_string = "(A22,I3)"
        else if (n < 10000) then
            format_string = "(A22,I4)"
        else if (n < 100000) then
            format_string = "(A22,I5)"
        else if (n < 1000000) then
            format_string = "(A22,I6)"
        endif

        write (plot_label ,format_string) 'Section 3.1, Ntrials: ', n
        maxValue = 8
        data(:) = 0
        data_binned(:) = 0
        bin_value = [1.0_dp, 2.0_dp, 3.0_dp, 4.0_dp, 5.0_dp, 6.0_dp, 7.0_dp, 8.0_dp ]

        do i=1,n
            call random_number(randomReal)
            randomInt = floor(randomReal * maxValue) + 1
            data(i) = randomInt 
            data_binned(randomInt) = data_binned(randomInt) + 1
        enddo

        data_binned = data_binned / real(n)
        
!        call plcol0(14)
        call plenv(xmin, xmax, ymin, ymax, 0, 0)
        call plbin(bin_value, data_binned, 1)
        call pljoin(1.0_dp, 0.0_dp, 1.0_dp, 0.125_dp)
        call pljoin(2.0_dp, 0.0_dp, 2.0_dp, 0.125_dp)
        call pljoin(3.0_dp, 0.0_dp, 3.0_dp, 0.125_dp)
        call pljoin(4.0_dp, 0.0_dp, 4.0_dp, 0.125_dp)
        call pljoin(5.0_dp, 0.0_dp, 5.0_dp, 0.125_dp)
        call pljoin(6.0_dp, 0.0_dp, 6.0_dp, 0.125_dp)
        call pljoin(7.0_dp, 0.0_dp, 7.0_dp, 0.125_dp)
        call pljoin(8.0_dp, 0.0_dp, 8.0_dp, 0.125_dp)

        call plstring(bin_value, true_value, '•')

        call pllab('Dice Side', 'Probability', trim(plot_label))
        
        call pladv(1)

    END SUBROUTINE solveproblem_3_1

END PROGRAM project_1
