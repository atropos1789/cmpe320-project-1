! Problem 3.1
!    for N in [ 240 2400 24000 240000 ]
!    1. Choose a random integer \in [1,8] N times
!    2. put all of the random trials into an array
!    3. compute the sample mean and sample variance of the array
!    4. plot a histogram of the array, and a stem plot of the theoretical values

! turn each experiment into a subroutine

PROGRAM problem_3_1
    use plplot
    implicit none 
    
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc
    integer, allocatable :: seed(:)
    integer              :: n
    
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    
    call random_seed(size = n)
    allocate(seed(n))
    call random_seed(get=seed)   

    call plgdev(driver)
    call plgfam(fam,num,bmax)
    call plinit()
    call plcol0(6)
    
    call solveproblem_3_1(240)
    call solveproblem_3_1(2400)
    call solveproblem_3_1(24000)
    call solveproblem_3_1(240000)

    call plend

CONTAINS

    SUBROUTINE solveproblem_3_1(n)
        integer, intent(in) :: n 
        integer :: i, maxValue, randomInt
        real :: randomReal
        real(kind=pl_test_flt) :: experiment_data(n) ! not supposed to use pl_test_flt, figure out what to use

        experiment_data(:) = 0
        maxValue = 8

        do i=1,n
            call random_number(randomReal)
            randomInt = floor(randomReal * maxValue) + 1
            experiment_data(i) = randomInt 
        enddo
        
        call plhist(experiment_data, 0.5_pl_test_flt, 8.5_pl_test_flt, maxValue, 0)
        call pladv(1)

    END SUBROUTINE solveproblem_3_1

END PROGRAM problem_3_1
