! Problem 3.2
! For N in 100, 1000, 1000
! For p1 in 0.5, 0.2, 0.7
! 1. Generate N 100-character binary strings with probability of 1 = p1
! 2. Determine the first 1 in each string
! 3. Determine the mean and standard deviation of the location of the first 1

! Use fortRAND to generate random numbers.

PROGRAM problem_3_2
    use fr_random
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none

    integer              :: n(3)
    real(dp)             :: p1(3)
    integer              :: i, j

    n = [ 100, 1000, 10000 ]
    p1 = [ 0.5_dp, 0.7_dp, 0.2_dp ]

    ! run the experiment for all values of n and p1
    ! note: needing 3 graphs per image will mean this needs to be rewritten
    do i = 1, size(n)
        do j = 1, size(p1)
            call do_problem_3_2( n(i), p1(j) )
        enddo
    enddo
    
CONTAINS

    SUBROUTINE do_problem_3_2(n, p1)
        integer, intent(in) :: n
        real(dp), intent(in) :: p1
        integer :: i, j
        real(dp) :: raw_data(n,100) !each col is a different string
        integer :: maxval
        integer :: data(n)
        real(dp), allocatable :: binned_data(:), bin_vals(:)
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance
        

        ! empty variables
        raw_data(:,:) = 0
        data(:) = 0
        data_mean = 0
        pop_mean = 0
        data_variance = 0
        pop_variance = 0
        maxval = 0

        ! print values of n, p1
        print *, '---------------------------'
        print *, 'n equals', n
        print *, 'p1 equals', p1
        
        ! create raw data
        do i = 1, n
            raw_data(i,:) = random(100) ! if i was smart, i'd call this once and map it onto a 2d array using linewrapping stuff
            do j = 1, 100
                if (raw_data(i,j) >= p1) then 
                    raw_data(i,j) = 0
                else
                    raw_data(i,j) = 1
                endif
            enddo
        enddo

        ! create data
        ! find first 1 in each row of the array
        col: do i = 1, n
            row: do j = 1, 100
                if ( raw_data(i,j) == 1 ) then
                    data(i) = j
                    exit row ! exit the do loop once the first 1 is found
                endif
            enddo row
        enddo col

        ! find largest value in data
        do i=1, size(data)
            if ( data(i) >= maxval ) maxval = data(i)
        enddo

        ! create array to hold binned data
        allocate(binned_data(maxval))
        allocate(bin_vals(maxval))
        binned_data(:) = 0

        ! bin data
        do i = 1, n
            binned_data(data(i)) = binned_data(data(i)) + 1
        enddo
        bin_vals = [ (i, i = 1, maxval) ]
        
        ! compute data mean
        data_mean = sum(data)/real(n)

        ! compute data variance 
        data_variance = sum((data_mean - data)**2) / real(n)
    
        ! compute population mean and variance
        pop_mean = 1 / p1
        pop_variance = ( 1 - p1 ) / ( p1**2 )  

        ! print values to console  
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'population mean:', pop_mean
        print *, 'population variance:', pop_variance

        ! graph data
        ! print binned histogram with bin_vals as the values and binned_data as the data
        ! also include a stem plot of analytical values.
        
    END SUBROUTINE do_problem_3_2

END PROGRAM problem_3_2
