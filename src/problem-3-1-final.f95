!> Problem 3.1
!>    for N in [ 240 2400 24000 240000 ]
!>    1. Choose a random integer \in [1,8] N times
!>    2. put all of the random trials into an array
!>    3. compute the sample mean and sample variance of the array
!>    4. plot a histogram of the array, and a stem plot of the theoretical values


PROGRAM problem_3_1
    use plplot
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

    !> General Variables
    integer :: i, j, k
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc

    !> Problem Parameters
    integer, parameter :: n(4) = [ 240, 2400, 24000, 240000 ]

    ! graph preparation
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    
    call plgdev(driver)
    call plspal0('cmap0_white_bg.pal')
    call plgfam(fam,num,bmax)
    call plinit()

    !> Do Problem
    do i = 1, size(n)
        call do_problem_3_1(n(i))
    enddo

CONTAINS

    SUBROUTINE do_problem_3_1(n)

        ! declare variables
        integer, intent(in) :: n 
        integer :: i
        integer :: data(n)
        real(dp), allocatable :: binned_data(:), bin_vals(:)
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance
        integer :: lowerbound, upperbound
        integer :: maxval, minval, range
        real(dp) :: maxvalReal, minvalReal
        real(dp) :: xmin, xmax, ymin, ymax
        real(dp) :: analytic_data(8)
        character(len=30) :: plot_label
        character(len=20) :: format_string

        ! initialize variables
        data(:) = 0
        data_mean = 0.0
        pop_mean = 0.0
        data_variance = 0.0
        pop_variance = 0.0
        lowerbound = 1
        upperbound = 8
        maxval = 0
        minval = 8
        maxvalReal = 0.0_dp
        minvalReal = 0.0_dp

        ! create raw data 
        data = random(lowerbound, upperbound, n)
        
        ! create data 
        ! unnecessary here

        ! find data bounds and range 
        do i = 1, size(data)
            if ( data(i) >= maxvalReal ) maxvalReal = data(i)
            if ( data(i) <= minvalReal ) minvalReal = data(i)
        enddo
        maxval = int(maxvalReal)
        minval = int(minvalReal)
        range = abs(minval) + abs(maxval)

        ! bin data
        allocate(binned_data(range))
        allocate(bin_vals(range))
        binned_data(:) = 0
        bin_vals = [(i + minval, i = 1, range)]
        do i = 1, n
            binned_data(data(i)) = binned_data(data(i)) + 1
        enddo
        binned_data = binned_data / real(n)

        ! create analytic data
        analytic_data(:) = 0.125_dp

        ! compute sample mean and variance
        data_mean = sum(data)/real(n)
        data_variance = sum((data_mean - data)**2) / real(n)

        ! compute population mean and variance
        pop_mean = 4.5_dp
        pop_variance = 5.25_dp

        ! print values to console
        print *, '---------------------------'
        print *, 'n equals', n
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'population mean:', pop_mean
        print *, 'population variance:', pop_variance
        print *, 'absolute error in mean', abs(pop_mean - data_mean)
        print *, 'absolute error in variance', abs(pop_variance - data_variance)

        ! setup graph
        xmin = 0.5_dp
        xmax = 8.5_dp
        ymin = 0.0_dp
        ymax = 0.2_dp
        if (n < 10) then
            format_string = "(A22,I1)"
        else if (n < 100) then
            format_string = "(A22,I2)"
        else if (n < 1000) then
            format_string = "(A22,I3)"
        else if (n < 10000) then
            format_string = "(A22,I4)"
        else if (n < 100000) then
            format_string = "(A22,I5)"
        else if (n < 1000000) then
            format_string = "(A22,I6)"
        endif
        write (plot_label ,format_string) 'Section 3.1, Ntrials: ', n
        call plcol0(15)
        call plenv(xmin, xmax, ymin, ymax, 0, 0)
        call pllab('Dice Side', 'Probability', trim(plot_label))

        !! graph data
        call plbin(bin_vals, binned_data, 1)
        
        !! graph analytic data
        call plcol0(10)
        call plstring(bin_vals, analytic_data, '•')
        do i = 1, maxval
            call pljoin( real(i, kind=dp), 0.0_dp, real(i, kind=dp), analytic_data(i))
        enddo

        call pleop()
        call plbop()

    END SUBROUTINE do_problem_3_1
    
END PROGRAM problem_3_1
