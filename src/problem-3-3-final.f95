PROGRAM problem_3_3
    use plplot
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

    !> General Variables
    integer :: i, j, k
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc
    integer :: maxval, minval, range, num_bins, rounded_size
    real(dp) :: maxvalReal, minvalReal, bin_size, legend_width, legend_height
    real(dp), allocatable :: binned_data(:) 
    real(dp), allocatable :: bin_vals(:)
    real(dp), allocatable :: analytic_data(:)
    real(dp) :: data_mean, pop_mean
    real(dp) :: data_variance, pop_variance
    real(dp) :: xmin, xmax, ymin, ymax
    character(len=30) :: plot_label
    character(len=20) :: format_string

    !> Problem Parameters
    integer, parameter :: n = 100000
    real(dp)        :: lambda = 0.7_dp
    real(dp) :: data(n), index


    data(:) = 0.0_dp
    data_mean = 0.0_dp
    pop_mean = 0.0_dp
    data_variance = 0.0_dp
    pop_variance = 0.0_dp
    minval = 0
    maxval = 0
    minvalReal = 0.0_dp
    maxvalReal = 0.0_dp
    bin_size = 0.5_dp

    ! graph preparation
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    call plgdev(driver)
    call plspal0('cmap0_white_bg.pal')
    call plgfam(fam,num,bmax)
    call plinit()

    ! create raw data 
    data = exponential_dist(n, lambda)

    ! create data (make half of the values negative)
    do i = 1, n/2
        data(i) = -1.0_dp * data(i)
    enddo

    ! find data bounds and range 
    do i = 1, size(data)
        if ( data(i) <= minvalReal ) minvalReal = data(i)
        if ( data(i) >= maxvalReal ) maxvalReal = data(i)
    enddo
    minval = int(minvalReal)
    maxval = int(maxvalReal)
    range = abs(minval) + abs(maxval)
    num_bins = int(real(range, kind=dp) / bin_size)

    ! bin data
    allocate(binned_data(num_bins), source=0.0_dp)
    allocate(bin_vals(num_bins), source=0.0_dp)
        
    bin_vals = [( bin_size * real(i, kind=dp) + real(minval, kind=dp), i = 1, num_bins )]
                
    do j = 1, size(data)
        rounded_size = nint(2.0*(data(j))) - 2*minval
        if (rounded_size <= size(binned_data)) binned_data(rounded_size) = binned_data(rounded_size) + 1.0
    enddo
    binned_data = 2 * binned_data / real(n)

    ! compute sample mean and variance
    data_mean = sum(data)/real(n, kind=dp)
    data_variance = sum((data_mean - data)**2) / real(n, kind=dp)

    ! compute population mean and variance
    pop_mean = 0
    pop_variance = 2.0 / lambda**2 

    ! construct analytic values
    allocate(analytic_data(num_bins))
        
    do i = 1, size(bin_vals)
        analytic_data(i) = 0.5_dp * lambda * exp( -1.0_dp * lambda * abs( bin_vals(i) ) )
    enddo

    ! print values to console
    print *, '---------------------------'
    print *, 'n equals', n
    print *, 'lambda equals', lambda
    print *, 'sample mean: ', data_mean
    print *, 'sample variance: ', data_variance
    print *, 'absolute error in mean', abs(pop_mean - data_mean)
    print *, 'absolute error in variance', abs(pop_variance - data_variance)
    print *, 'population mean:', pop_mean
    print *, 'population variance:', pop_variance

    ! setup graph
    xmin = real(minval, kind=dp) + (bin_size / 2.0_dp)
    xmax = real(maxval, kind=dp) + (bin_size / 2.0_dp)
    ymin = 0.0_dp
    ymax = 0.4_dp

    if (n < 10) then
        format_string = "(A22,I1)"
    else if (n < 100) then
        format_string = "(A22,I2)"
    else if (n < 1000) then
        format_string = "(A22,I3)"
    else if (n < 10000) then
        format_string = "(A22,I4)"
    else if (n < 100000) then
        format_string = "(A22,I5)"
    else if (n < 1000000) then
        format_string = "(A22,I6)"
    endif

    write (plot_label, format_string) 'Section 3.3, Ntrials: ', n
    call plcol0(15)
    call plenv(xmin, xmax, ymin, ymax, 0, 0)
    call pllab('', 'Probability', trim(plot_label))


    ! ! graph data
    call plbin(bin_vals, binned_data, 1)
!    call plbin(bin_vals, analytic_data, 1)

    ! ! graph analytic data
    call plcol0(10)
    call plstring(bin_vals, analytic_data, '•')
    do i = 1, num_bins
        index = real(i, kind=dp) * bin_size + real(minval, kind=dp)
        call pljoin( index, 0.0_dp, index, analytic_data(i))
    enddo     

!    call pllegend( legend_width, & ! p_legend_width
!                   legend_height, & ! p_legend_height
!                   0, & ! opt
!                   0, & ! position
!                   0.0_dp, & ! x 
!                   0.0_dp, & ! y
!                   0.1_dp, & ! plot_width
!                   15, & ! bg_color
!                   15, & ! bb_color
!                   0, & ! bb_style
!                   0, & ! nrow
!                   0, & ! ncolumn
!                   0, & ! nlegend
!                   [ 1, 1 ], & ! opt_array
!                   1.0_dp, & ! text_offset
!                   1.0_dp, & ! text_scale
!                   2.0_dp, & ! text_spacing
!                   1.0_dp, & ! text_justification
!                   [ 15, 10 ], & ! text_colors
!                   [ 'Sample Data  ', 'Analytic Data' ], & ! text
!                   [ 1, 1 ], & ! box_colors
!                   [ 1, 1 ], & ! box_patterns
!                   [ 1.0_dp, 1.0_dp ] , & ! box_scales
!                   [ 1.0_dp, 1.0_dp ], & ! box_line_widths
!                   [ 15, 10 ], & ! line_colors
!                   [ 1, 1], & ! line_styles
!                   [ 1.0_dp, 1.0_dp], & ! line_widths
!                   [ 0,0 ], & ! symbol_colors
!                   [ 0.0_dp, 0.0_dp ], & ! symbol_scales
!                   [ 0, 0 ], & ! symbol_numbers
!                   [ ' ', ' ' ] )
      

    
    !! Finish up
    !call pladv(1)
    call pleop()
    call plbop()

    ! call plend()
    
END PROGRAM problem_3_3
