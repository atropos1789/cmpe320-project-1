PROGRAM problem_3_5
    use plplot
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

    !> General Variables
    integer :: i, j, k
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc
    integer :: maxval, minval, range, num_bins, rounded_size
    real(dp) :: maxvalReal, minvalReal, bin_size, legend_width, legend_height
    real(dp), allocatable :: binned_data(:) 
    real(dp), allocatable :: bin_vals(:)
    real(dp), allocatable :: analytic_data(:)
    real(dp) :: data_mean, pop_mean
    real(dp) :: data_variance, pop_variance
    real(dp) :: xmin, xmax, ymin, ymax
    character(len=30) :: plot_label
    character(len=20) :: format_string
    integer :: num_trials
    real(dp) :: scaled_trials
    real(dp) :: prob_trials
    real(dp) :: analytic_probability, x1, x2

    !> Problem Parameters
    integer, parameter :: n = 100000
    real(dp), parameter :: mean = 2.0_dp
    real(dp), parameter :: variance = 6.25_dp
    real(dp), parameter :: pi = 4.0_dp * atan(1.0_dp)
    real(dp) :: data(n), index


    data(:) = 0.0_dp
    data_mean = 0.0_dp
    pop_mean = 0.0_dp
    data_variance = 0.0_dp
    pop_variance = 0.0_dp
    minval = 0
    maxval = 0
    minvalReal = 0.0_dp
    maxvalReal = 0.0_dp
    bin_size = 0.5_dp

    ! graph preparation
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    call plgdev(driver)
    call plspal0('cmap0_white_bg.pal')
    call plgfam(fam,num,bmax)
    call plinit()

    ! create raw data 
    data = normal_dist(n)

    ! create data (scale by sqrt(variance) and shift by mean
    data = shift_normal_distributed(data, mean, sqrt(variance))

    ! find data bounds and range 
    do i = 1, size(data)
        if ( data(i) <= minvalReal ) minvalReal = data(i)
        if ( data(i) >= maxvalReal ) maxvalReal = data(i)
    enddo
    minval = int(minvalReal)
    maxval = int(maxvalReal)
    range = abs(minval) + abs(maxval)
    num_bins = int(real(range, kind=dp) / bin_size)

    ! bin data
    allocate(binned_data(num_bins), source=0.0_dp)
    allocate(bin_vals(num_bins), source=0.0_dp)
    bin_vals = [( bin_size * real(i, kind=dp) + real(minval, kind=dp), i = 1, num_bins )]
    do j = 1, size(data)
        rounded_size = nint(2.0*(data(j))) - 2*minval
        if (rounded_size <= size(binned_data)) binned_data(rounded_size) = binned_data(rounded_size) + 1.0
    enddo
    
    ! before normalizing, do stuff for problem 3.6
    num_trials = 0
    do i = 1, num_bins
        if ((bin_vals(i) .le. 4.5) .and. (bin_vals(i) > -0.5)) then
            num_trials = num_trials + binned_data(i)
        endif
    enddo
    scaled_trials = real(num_trials, kind=dp) / real(n, kind=dp)

    ! normalize data
    binned_data = 2.0_dp * binned_data / real(n, kind=dp)

    ! more problem 3.6 stuff
    prob_trials = 0.0_dp
    do i = 1, num_bins
        if ((bin_vals(i) .le. 4.5) .and. (bin_vals(i) > -0.5)) then
            prob_trials = prob_trials + 0.5_dp * binned_data(i)
        endif
    enddo

    
    
    ! compute sample mean and variance
    data_mean = sum(data)/real(n, kind=dp)
    data_variance = sum((data_mean - data)**2) / real(n, kind=dp)

    ! compute population mean and variance
    pop_mean = mean
    pop_variance = variance

    ! construct analytic values
    allocate(analytic_data(num_bins))
    do i = 1, size(bin_vals)
        analytic_data(i) = ( 1.0 / sqrt(2.0 * pi * variance) ) * &
                           exp((-( bin_vals(i) - mean )**2) / (2 * variance))
    enddo

    ! find analytic CDF
    ! NOTE: Using fortran erf(), only present in Fortran 2008 and newer
    ! NOTE: will first compute the CDF at x=4.5, and add the CDF at x=-0.5 to that
    x1 = (4.5_dp - mean)/sqrt( 2.0_dp * variance)
    x2 = (-0.5_dp - mean)/sqrt( 2.0_dp * variance)
    analytic_probability = 0.5_dp * (erf(x1) - erf(x2))

    ! print values to console
    print *, '---------------------------'
    print *, 'n equals', n
    print *, 'mu equals', mean
    print *, 'sigma squared equals', variance
    print *, 'sample mean: ', data_mean
    print *, 'sample variance: ', data_variance
    print *, 'absolute error in mean:', abs(pop_mean - data_mean)
    print *, 'absolute error in variance:', abs(pop_variance - data_variance)
    print *, 'population mean:', pop_mean
    print *, 'population variance:', pop_variance
    print *, 'Number of trials between -0.5 and 4.5, scaled', scaled_trials
    print *, 'Sample Pr[-0.5 <= X < 4.5]', prob_trials
    print *, 'Analytical Pr[-0.5 <= X < 4.5]', analytic_probability

    ! setup graph
    xmin = real(minval, kind=dp) + (bin_size / 2.0_dp)
    xmax = real(maxval, kind=dp) + (bin_size / 2.0_dp)
    ymin = 0.0_dp
    ymax = 0.3_dp

    if (n < 10) then
        format_string = "(A22,I1)"
    else if (n < 100) then
        format_string = "(A22,I2)"
    else if (n < 1000) then
        format_string = "(A22,I3)"
    else if (n < 10000) then
        format_string = "(A22,I4)"
    else if (n < 100000) then
        format_string = "(A22,I5)"
    else if (n < 1000000) then
        format_string = "(A22,I6)"
    else if (n < 10000000) then
        format_string = "(A22,I7)"
    else if (n < 100000000) then
        format_string = "(A22,I8)"
    endif

    write (plot_label, format_string) 'Section 3.5, Ntrials: ', n
    call plcol0(15)
    call plenv(xmin, xmax, ymin, ymax, 0, 0)
    call pllab('', 'Probability', trim(plot_label))


    !! graph data
    call plbin(bin_vals, binned_data, 1)

    !! graph analytic data
    call plcol0(10)
    call plstring(bin_vals, analytic_data, '•')
    do i = 1, num_bins
        index = real(i, kind=dp) * bin_size + real(minval, kind=dp)
        call pljoin( index, 0.0_dp, index, analytic_data(i))
    enddo     

    !! Finish up
    call pleop()
    call plbop()
    
END PROGRAM problem_3_5
