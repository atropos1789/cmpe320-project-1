PROGRAM problem_3_2
    use plplot
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

    !> General Variables
    integer :: i, j, k
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc

    !> Problem Parameters
    integer, parameter :: n(3) = [ 100, 1000, 10000 ]
    real(dp), parameter :: p1(3) = [ 0.5_dp, 0.7_dp, 0.2_dp ]

    ! graph preparation
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    
    call plgdev(driver)
    call plspal0('cmap0_white_bg.pal')
    call plgfam(fam,num,bmax)
    call plinit()

    !> Do Problem
    do i = 1, size(p1)
        do j = 1, size(n)
            call do_problem_3_2( n(j), p1(i) )
        enddo
    enddo

    call plend()


CONTAINS

    SUBROUTINE do_problem_3_2(n, p1)

        ! declare variables
        integer, intent(in) :: n
        real(dp), intent(in) :: p1
        real(dp) :: data_mean, pop_mean
        real(dp) :: data_variance, pop_variance
        real(dp) :: raw_data(n,100), data(n)
        real(dp), allocatable :: binned_data(:), bin_vals(:), analytic_data(:)
        integer :: i, j, k, maxval
        real(dp) :: xmin, xmax, ymin, ymax
        character(len=30) :: plot_label
        character(len=20) :: format_string

        ! initialize variables
        raw_data(:,:) = 0
        data(:) = 0
        data_mean = 0
        pop_mean = 0
        data_variance = 0
        pop_variance = 0
        maxval = 0

        ! create raw data 
        do i = 1, n
            raw_data(i,:) = random(100) ! if i was smart, i'd call this once and map it onto a 2d array using linewrapping stuff
            do j = 1, 100
                if (raw_data(i,j) >= p1) then 
                    raw_data(i,j) = 0
                else
                    raw_data(i,j) = 1
                endif
            enddo
        enddo

        ! create data
        col: do i = 1, n
            row: do j = 1, 100
                if ( raw_data(i,j) == 1 ) then
                    data(i) = j
                    exit row ! exit the do loop once the first 1 is found
                endif
            enddo row
        enddo col

        ! find data bounds and range 
        do i = 1, size(data)
            if ( int(data(i)) >= maxval ) maxval = int(data(i))
        enddo

        ! bin data
        allocate(bin_vals(maxval), source=0.0_dp)
        bin_vals = [ (real(i, kind=dp), i = 1, maxval) ]
        allocate(binned_data(maxval), source=0.0_dp)
        do i = 1, n
            binned_data(int(data(i))) = binned_data(int(data(i))) + 1
        enddo
        binned_data = binned_data / real(n)

        ! compute sample mean and variance
        data_mean = sum(data)/real(n)
        data_variance = sum((data_mean - data)**2) / real(n)

        ! compute population mean and variance
        pop_mean = 1 / p1
        pop_variance = ( 1 - p1 ) / ( p1**2 )  

        ! construct analytic data
        allocate(analytic_data(maxval))
        
        do i = 1, size(bin_vals)
            analytic_data(i) = p1 * (1.0_dp - p1)**(i - 1)
        enddo


        ! print values to console
        print *, '---------------------------'
        print *, 'n equals', n
        print *, 'p1 equals', p1
        print *, 'sample mean: ', data_mean
        print *, 'sample variance: ', data_variance
        print *, 'absolute error in mean:', abs(pop_mean - data_mean)
        print *, 'absolute error in variance:', abs(pop_variance - data_variance)
        print *, 'population mean:', pop_mean
        print *, 'population variance:', pop_variance

        ! set-up graph
        xmin = 0.0_dp + 0.5_dp
        xmax = real(maxval) + 0.5_dp
        ymin = 0.0_dp
        if ( p1 == 0.7_dp) then
            ymax = 1.0_dp
        else if (p1 == 0.5_dp) then
            ymax = 0.6_dp
        else
            ymax = 0.3_dp
        endif

        if (n < 10) then
            format_string = "(A22,I1)"
        else if (n < 100) then
            format_string = "(A22,I2)"
        else if (n < 1000) then
            format_string = "(A22,I3)"
        else if (n < 10000) then
            format_string = "(A22,I4)"
        else if (n < 100000) then
            format_string = "(A22,I5)"
        else if (n < 1000000) then
            format_string = "(A22,I6)"
        endif

        write (plot_label, format_string) 'Section 3.2, Ntrials: ', n
        call plcol0(15)
        call plenv(xmin, xmax, ymin, ymax, 0, 0)
        call pllab('', 'Probability', trim(plot_label))

        ! ! graph data
        call plbin(bin_vals, binned_data, 1)

        ! ! graph analytic data
        call plcol0(10)
        call plstring(bin_vals, analytic_data, '•')
        do i = 1, maxval
            call pljoin( real(i, kind=dp), 0.0_dp, real(i, kind=dp), analytic_data(i))
        enddo

    END SUBROUTINE do_problem_3_2


END PROGRAM problem_3_2
