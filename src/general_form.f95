PROGRAM problem_3
    use plplot
    use fr_random
    use fr_sampling
    use, intrinsic :: iso_c_binding, only: sp=>c_float, dp=>c_double
    implicit none 

    !> General Variables
    integer :: i, j, k
    character(len=80) :: driver
    integer :: fam, num, bmax
    integer :: plparseopts_rc

    !> Problem Parameters


    ! graph preparation
    plparseopts_rc = plparseopts(PL_PARSE_FULL)
    if(plparseopts_rc .ne. 0) stop "plparseopts error"
    
    call plgdev(driver)
    call plspal0('cmap0_black_on_white.pal')
    call plgfam(fam,num,bmax)
    call plinit()

    !> Do Problem


CONTAINS

    SUBROUTINE do_problem_3

        ! declare variables

        ! initialize variables

        ! create raw data 

        ! create data

        ! find data bounds and range 

        ! bin data

        ! compute sample mean and variance

        ! compute population mean and variance

        ! print values to console

        ! set-up graph

        ! ! graph data

        ! ! graph analytic data


    END SUBROUTINE do_problem_3


END PROGRAM problem_3
