FCC = /usr/bin/gfortran
FFLAGS = -g -std=f2008 -Wpedantic -Wall -Warray-bounds  -Wc-binding-type  -Wcharacter-truncation  -Wconversion  -Wdo-subscript  -Wfunction-elimination  -Wimplicit-interface  -Wimplicit-procedure -Wintrinsic-shadow -Wintrinsics-std -Wline-truncation -Wno-align-commons -Wno-overwrite-recursive -Wno-tabs -Wreal-q-constant -Wsurprising -Wunderflow  -Wunused-parameter
LDFLAGS = $(shell pkg-config --cflags --libs plplot-fortran)

PROB1VERSION = final
PROB2VERSION = final
PROB3VERSION = final
PROB4VERSION = final
PROB5VERSION = final

problem-3-1:
	$(FCC) $(FFLAGS) -o bin/problem-3-1-$(PROB1VERSION) src/problem-3-1-$(PROB1VERSION).f95 bin/random.o bin/sampling.o $(LDFLAGS)
	bin/problem-3-1-$(PROB1VERSION) -fam -fbeg 1 -finc 1 -px 1 -py 1

problem-3-2:
	$(FCC) $(FFLAGS) -o bin/problem-3-2-$(PROB2VERSION) src/problem-3-2-$(PROB2VERSION).f95 bin/random.o bin/sampling.o $(LDFLAGS)
	bin/problem-3-2-$(PROB2VERSION) -fam -fbeg 1 -finc 1 -py 3
	
problem-3-3:
	$(FCC) $(FFLAGS) -o bin/problem-3-3-$(PROB3VERSION) src/problem-3-3-$(PROB3VERSION).f95 bin/random.o bin/sampling.o $(LDFLAGS)
	bin/problem-3-3-$(PROB3VERSION) -fam -fbeg 1 -finc 1 -px 1 -py 1
	
problem-3-4:
	$(FCC) $(FFLAGS) -o bin/problem-3-4-$(PROB4VERSION) src/problem-3-4-$(PROB4VERSION).f95 bin/random.o bin/sampling.o $(LDFLAGS)
	bin/problem-3-4-$(PROB4VERSION) -fam -fbeg 1 -finc 1 -px 1 -py 1
problem-3-5:
	$(FCC) $(FFLAGS) -o bin/problem-3-5-$(PROB5VERSION) src/problem-3-5-$(PROB5VERSION).f95 bin/random.o bin/sampling.o $(LDFLAGS)
	bin/problem-3-5-$(PROB5VERSION) -fam -fbeg 1 -finc 1 -px 1 -py 1
    
.PHONY: clean
clean:
	rm -f bin/*
