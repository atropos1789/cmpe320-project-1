# Project 1 Notes

## Fortran Specifics

### RNG

https://cyber.dabamos.de/programming/modernfortran/random-numbers.html

GNU specific extension:
https://simplyfortran.com/docs/compiler/RAND.html

https://masuday.github.io/fortran_tutorial/random.html

https://fortranwiki.org/fortran/show/random_number

### Misc

https://riptutorial.com/fortran/example/10042/the-intent-of-dummy-arguments
https://stackoverflow.com/questions/22578987/interface-mismatch-higher-order-functions


## Submission

two pdfs, 

#1 has the main writing, figures, etc
#2 has the code (as PDF)

## Code structure

1. call procedures for each instance of each problem (expect 3 or 9 total calls per problem)

2. procedure creates (unbinned) data, calls a function to bin the data 

3. prints out the population and sample mean and variances 

4. exports image plots 
