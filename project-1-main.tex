% CMPE320
% Project 1
% Kira Singla

\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}

\usepackage{fontspec}
\setmainfont{Liberation Serif}

\usepackage{polyglossia}
\setmainlanguage{english}

\usepackage{geometry}
\geometry{letterpaper, total={6.5in,9in}}
 
\usepackage{graphicx}
\usepackage{subcaption}
\graphicspath{ {./images/} }

\DeclareMathOperator\erf{erf}

\begin{document}

\begin{flushleft}
CMPE320 Probability, Statistics, and Random Processes \\
Kira Singla \\
They/Them \\
ksingla1@umbc.edu
\end{flushleft}

\section*{Project 1: Writeup}

\subsection*{Introduction}

Most computers do not have the ability to generate perfectly random numbers, instead using psuedorandom number generators. 
These approximate random numbers to an imperfect degree, however this project will show that even when using these psuedorandom numbers, it is still possible to create datasets that have properties closely mirroring the analytic distributions that they are designed to mimic.
This project will also show that as the number of samples from a random number generator increases within a dataset, the properties of the dataset will be more similar to the properties of the analytic distribution. 


\subsection*{Simulation and Discussion}

\subsubsection*{Problem 3.1}

\begin{table}[h!]
    \centering
    \begin{tabular} { |c|c|c|c|c| }
        \hline
        $N$    & $\mu_{\text{sample}}$ & $\sigma^2_{\text{sample}}$ & $\vert \mu - \mu_{\text{sample}}\vert$ & $\vert \sigma^2 - \sigma^2_{\text{sample}}\vert$ \\
        \hline
        240    & 4.271  & 5.456  & 2.292E-1  & 2.058E-1 \\
        2400   & 4.560  & 5.111  & 6.043E-2  & 1.387E-1 \\
        24000  & 4.487  & 5.231  & 1.263E-2  & 1.933E-2 \\
        240000 & 4.506  & 5.248  & 6.479E-3  & 1.875E-3 \\
        \hline
    \end{tabular}
    \caption{Data for Problem 3.1}
    \label{table:p1}
\end{table}

Based on the results from Table \ref{table:p1}, it can clearly be observed that as N increases, the sample mean of the data approaches the population mean (which is known to be 4.5), and the sample variance of the data approaches the population variance (known to be 5.25). 
For \(N = 240000\), this data is very accurate, with \( \vert \mu - \mu_{\text{sample}}\vert \sim 10^{-4}\) and \( \vert \sigma^2 - \sigma^2_{\text{sample}}\vert \sim 10^{-3}\). 
Visually, Figure \ref{fig:p1} shows similar information, with each graph becoming more uniform as $N$ increases, and each bin being closer in value to the plotted PMF. 
Overall, this demonstrates that the dataset created from $N$ trials has the most similarity to the theoretical distribution when $N$ has its greatest value.


\newpage
\subsubsection*{Problem 3.2}

\begin{table}[h!]
    \centering
    \begin{tabular} { |c|c|c|c|c|c|c|c| }
        \hline
        $N$   & $p_1$  & $\mu_{\text{sample}}$  & $\sigma^2_{\text{sample}}$ & $\vert \mu - \mu_{\text{sample}}\vert$ & $\vert \sigma^2 - \sigma^2_{\text{sample}}\vert$ & $\mu$ & $\sigma^2$ \\
        \hline
        100   & 0.5  & 2.040  & 1.858     & 4.000E-2  & 1.416E-1 & 2.000 & 2.000 \\
        1000  & 0.5  & 1.974  & 1.759     & 2.600E-2  & 2.407E-1 & & \\ 
        10000 & 0.5  & 2.023  & 2.088     & 2.350E-2  & 8.835E-2 & &\\
        \hline
        100   & 0.7  & 1.360  & 4.704E-1  & 6.857E-2  & 1.418E-1 & 1.420 & 6.122E-1 \\
        1000  & 0.7  & 1.417  & 5.851E-1  & 1.157E-2  & 2.713E-2 & & \\
        10000 & 0.7  & 1.430  & 5.833E-1  & 1.529E-3  & 2.893E-2 & &\\
        \hline
        100   & 0.2  & 4.870  & 1.593E+1  & 1.130E-1  & 4.067    & 5.000 & 2.000E+1 \\
        1000  & 0.2  & 5.440  & 2.513E+1  & 4.400E-1  & 5.126 & & \\
        10000 & 0.2  & 5.009  & 2.049E+1  & 8.900E-3  & 4.926E-1 & &\\
        \hline
    \end{tabular}
    \caption{Data for Problem 3.2}
    \label{table:p2}
\end{table}

Using the provided mean and variance formulas, the population mean and variance has been calculates for each value of $p_1$, and is visible in Table \ref{table:p2}. 
Based on the description of this experiment, it is a Geometric Trial of Type 1, so the PMF can be calculated from \( f_K[k] = p_1 ( 1 - p_1 )^k,\, k \in \mathbb{N} \).  
The absolute error in both mean and variance is relatively high for all 9 datasets, suggesting that Geometric distributions do not converge as quickly as uniform distributions, though it is also good to note that the uniform experiment was conducted with an order of magnitude more trials than this experiment. 
The mean error only gets to \( \sim 10^{-3} \) for the cases when \( N = 10000,\, p_1 = 0.7 \) and \( N = 10000,\, p_1 = 0.2 \), and the variance error never goes below \( \sim 10^{-2} \).

For all values of \(p_1\), the histograms shown in Figure \ref{fig:p2} have a roughly similar shape, trending as an exponentially decreasing curve, which is what should be expected given the PMF. 
This only exception to this is the graph where \(p_1 = 0.2,\, N = 100\), as this shows a much "bumpier" curve that is relatively far away from the PMF that it should be ideally similar to, as there are both many probable values that can be selected and few total samples that can be taken.
For \( p_1 = 0.7 \), the histograms have very few bins, whereas for \( p_1 = 0.2 \) there are many bins.
Intuitively, this is very reasonable, as the chance of getting a 1 in the first position is already 70\%, and the chance of getting a 0 in the first position and a 1 in the second position is \( 0.3(0.7) =\) 21\%. 
This means that the chance of selecting a value of either 1 or 2 is 91\%, which is enormous, so the chance of selecting a value even 3 or higher is only 9\%.
The opposite is true for \(p_1 = 0.2\), where the chance of getting a 1 is so low that there is a \( 0.2(0.8)^9 \approx \) 3\% chance that the first 1 will not appear until the 10th element of the string.


\newpage
\subsubsection*{Problem 3.3}

\begin{table}[h!]
    \centering
    \begin{tabular} { |c|c|c|c|c| }
        \hline
        $N$    & $\mu_{\text{sample}}$ & $\sigma^2_{\text{sample}}$ & $\vert \mu - \mu_{\text{sample}}\vert$ & $\vert \sigma^2 - \sigma^2_{\text{sample}}\vert$ \\
        \hline
        100    &  5.785E-2  & 4.791  & 5.785E-2  & 7.096E-1 \\
        1000   & -3.286E-2  & 4.478  & 3.287E-2  & 3.965E-1 \\
        100000 &  1.796E-3  & 4.069  & 1.795E-3  & 1.260E-2 \\
        \hline
    \end{tabular}
    \caption{Data for Problem 3.3}
    \label{table:p3}
\end{table}

This problem samples from a symmetric exponential distribution of \( \lambda = 0.7 \). Using this value, the population mean and variance can be identified: \(\mu = 0\) and \(\sigma^2 = 4.0816\), which allows for comparison of the sample and population values. The sample mean and variance, as well as their absolute errors, are found in \ref{fig:p3}, and they show that the absolute error in both quantities decreases as $N$ increase. 

The normalized histograms must be calculated so that the sum of the areas of each bar is equal to 1.00, because the sum of the probability of each event must be equal to 1, but it currently 
Each bar has a width of 0.5, and a height of n. 
It is known that the sum of these n values is the total number of trials, so the area of the un-normalized histogram is \(0.5(N)\). 
Thus in order to obtain a normalized histogram which has a total area of 1, this area must be multiplied by \( \frac{2}{N} \).
This translates to multiplying the height of each bar by \( \frac{2}{N} \), thus this is the scaling factor needed to obtain a normalized histogram for a bin width of 0.5. 
This also connects with the comparison of PMFs and PDFs, in this case by appealing to the fact that both must have an integral of 1.
The sample means and variances of the different trials show a monotone decrease in absolute error as $N$ increases, with the lowest errors being observed in the case where \( N = 100000 \). 
The graphs, shown in Figure \ref{fig:p3}, demonstrate this convergence, as the case where \(N=100\) resembles the PDF only poorly, whereas the case where \(N=100000\) resembles the PDF almost exactly. 
There are examples of random variance producing some unusual behavior: the graph of \(N=1000\) does not have a distinct peak at \(k=0\), even though the PDF implies it should have one.
This is a demonstration of the fact that the convergence of the dataset to the PDF is inexact and there can still be a dataset with high $N$ that shows behavior not shown in the PDF. 
Ultimately, a random sample, even if from a non-uniform distribution, is still random, and there can only be a finite amount of confidence that a dataset will perfectly reflect the analytic properties of the PDF. 


\newpage
\subsubsection*{Problem 3.4}

\begin{table}[h!]
    \centering
    \begin{tabular} { |c|c|c|c|c| }
        \hline
        $N$    & $\mu_{\text{sample}}$ & $\sigma^2_{\text{sample}}$ & $\vert \mu - \mu_{\text{sample}}\vert$ & $\vert \sigma^2 - \sigma^2_{\text{sample}}\vert$ \\
        \hline
        100    & -6.068E-2  & 9.138E-1  & 6.068E-2  & 8.617E-2 \\
        1000   & -1.976E-2  & 9.993E-1  & 1.977E-2  & 7.096E-4 \\
        100000 & -3.692E-3  & 9.968E-1  & 3.693E-3  & 3.164E-3 \\
        \hline
    \end{tabular}
    \caption{Data for Problem 3.4}
    \label{table:p4}
\end{table}

As in the previous problem, a scaling factor of \( \frac{2}{N} \) must be applied to the raw data produced by the experiment in order to get a normalized dataset in which each event represents a percent of the area of the figure equal to its probability. 
This is done for the same reasons as in the previous problem, as it is what produces a dataset that can be directly compared to the analytical PDF. 
The error in the sample mean and variance is much more unusual for this experiment than previous experiments, and further demonstrates that experiments with higher $N$ do not necessarily have statistics closer to the PDFs they represent.
Specifically, the error in the variance for \(N=1000\) is much lower than for \(N=100000\). 
The graphs in Figure \ref{fig:p4} are well behaved and closely match the analytical values in terms of shape. 
They display differences in the values themselves, especially for $N=100$, however this is to be expected given the few number of trials. 
The well-behaved nature of these graphs can be understood as a function of the fact that their bin size (which is still identical to the bin size used in 3.3 and 3.5) is a higher percentage over the overall range of the data, due to the variance being lower than in other datasets, except the Laplace distribution with $\lambda=0.7$. 


\subsubsection*{Problem 3.5}

\begin{table}[h!]
    \centering
    \begin{tabular} { |c|c|c|c|c| }
        \hline
        $N$    & $\mu_{\text{sample}}$ & $\sigma^2_{\text{sample}}$ & $\vert \mu - \mu_{\text{sample}}\vert$ & $\vert \sigma^2 - \sigma^2_{\text{sample}}\vert$ \\
        \hline
        100    & 1.978  & 6.677  & 2.189E-2  & 4.268E-1 \\
        1000   & 2.029  & 6.402  & 2.893E-2  & 1.523E-1 \\
        100000 & 1.986  & 6.237  & 1.374E-2  & 1.286E-2 \\
        \hline
    \end{tabular}
    \caption{Data for Problem 3.5}
    \label{table:p5}
\end{table}

Everything that can be said about problem 3.4 can also be said about problem 3.5, because the source code is essentially identical. 
This includes the scaling of the data and the reasoning behind the scaling of the data, so it will not be repeated here. 
The only procedural difference is that the random values, which were sourced from \( N(0,1) \) had to be multiplied by the square root of the variance and then summed with the mean. 
Table \ref{table:p5} shows that the error in both mean and variance generally decreases as $N$ increases, however this is not a rule, and the error in the mean for $N=1000$ is greater than for $N=100$. 
Graphically, Figure \ref{fig:p5} shows that this data looks "bumpier" than the data from in Figure \ref{fig:p4}, which is likely due to the variance of this data being greater, as that increases the number of bins that a random selection will likely be in.
However, once $N=100000$ is reached, the bumpiness is nowhere to be seen.
This "bumpiness" can be connected to a previous observation that the data in Figure \ref{fig:p4} has much fewer bins, so there is less resolution with which the individual values can be analyzed. 
This almost certainly obscures some of the "noise" and contributes to the smooth graph.


\newpage
\subsubsection*{Problem 3.6}

\begin{table}[h!]
    \centering
    \begin{tabular} { |c|c|c|c| }
        \hline
        $N$     & Scaled Unnormalized Data  & Normalized Data & Absolute Error \\
        \hline
        100     & 6.900E-1  & 6.900E-1  & 7.700E-3 \\
        1000    & 6.740E-1  & 6.740E-1  & 8.300E-3 \\ 
        100000  & 6.796E-1  & 7.696E-1  & 2.700E-3 \\
        \hline
    \end{tabular}
\end{table}

The analytic error is found computed using the fortran \texttt{erf()} intrinsic, as \[ Pr[ -0.5 \le x < 4.5 ] = 0.5\erf( \frac{4.5-2}{\sqrt{2*6.25}} ) - 0.5\erf( \frac{-0.5-2}{\sqrt{2*6.25}} ) = 6.823\text{E}-1,\] and from this the error of the estimations using the dataset are calculated. 
It is also of note that the scaled unnormalized data and the normalized data produced identical results to \(O(10^8)\), but this is unsurprising given that they are mathematically identical, only varying in the sequence in which certain operations were applied. 
Overall, this shows that the probability of a datapoint being between -0.5 and 4.5 converges to the actual value as $N$ increases.
This would likely converge faster if the bin widths were decreased, because as it, there is no bin that ends at -0.5 or 4.5, they are centered on these values, so what is actually being counted is -0.75 to 4.75, which are all the values that would be contained in the bins from -0.5 to 4.5.
Decreasing the bin width would decrease the overshoot in each direction, 
Further, this provides a demonstration of a very important identity in statistics: \( Pr[ \mu - \sigma \le x < \mu + \sigma ] \approx 0.6823 \), as the values -0.5 and 4.5 are evenly spaced from the mean of 2, at a distance of 2.5, which is the square root of the variance. 


\subsection*{What I Learned}

\quad\quad This project has been a massive learning experience for me, although a lot of it has been because I made the project substantially harder than it needed to be by choosing to use a compiled programming language (Fortran 2008) instead of using an interpreted one such as MATLAB or Python. 
This decision was made in equal parts because of my curiosity about compiled languages and my general desire to operate on as low of a level as possible when programming. 
In the past I have used Python and MATLAB for research projects, but found that the rapid development process made it very easy to lose a stable development branch and have to spend time backtracking. 
In this project, I opted to use a compiled language where it would be easier to separate the source files and the binaries, and to make new copies of the source for each time I compiled. 
I learned a lot about research documentation while working in this way, including how to maintain several active development branches for when I have multiple ideas I want to play around with, and how to manage a more involved build process through the use of Makefiles. 
I also had to learn about (and compile!) a whole new library to use for graphing with Fortran, called PLPlot.
This itself was a large challenge and it took me time to familiarize myself with the tool, but it taught me a lot about using software libraries because the library is not bound to any specific language, so I had to interpret the documentation for use with Fortran. 

Fortran does not have intrinsics to generate numbers from distributions, it only has a function to generate a random float between 0 and 1. 
In the process of finding tools to obtain a sample from a distribution, I learned about the mathematics of transforming distributions.
In effect, I had to find a way to transform a uniform distribution into other types of distributions. 
I was able to find source code that demonstrated the appropriate transformations for the exponential and normal distributions, and learned that doing this for normal distributions is actually not that straightforward, and can be done through a variety of algorithms of varying accuracy, including the Box Muller Algorithm (which was the default of the  library I ended up using). 
Being able to go through the code and documentation for these libraries taught me that a lot of these algorithms are very new and the code itself references academic papers published within my lifetime. 
This is very motivating for me personally, because I am interested in the application of academic discoveries to computer algorithms, and I find myself motivatved to follow the example of a lecturer hosted recently, who has published papers on an algorithm and coded several implementations of it that are freely available. 
Now that I've learned how to handle versions of my code more strictly, I feel more comfortable using interpreted languages for projects, however I am still not a fan of Python, so I will be chosing between Fortran and Julia (and potentially C) for future projects.  

I have already taken a statistics class and am generally familiar with the idea of convergence, so I did not find it surprising that the histograms approached the PMFs as N was increased. 
However, there were several instances where this did not happen, showing that this convergence is not monotonic by necessity.
Further, there was at least one instance of a histogram not closely representing the PMF, Problem 3.3 with \( N = 1000 \) didn't peak nearly as high in the center as other values of $N$ did.
I spent a lot of time trying to debug and search for possible bugs in my code, but this same code produced a perfect output for \( N = 100000 \) so I am lead to believe it was a fluke of statistics. 
Throughout this project I have had to do a lot of debugging of times when I had mishandled types in some way or another that created an artificial skew in the data, but I am confident that my code is free of this now. 
In total I cannot estimate how long I have worked on this project because much of my time learning to use tools for this project is applicable to other projects I am working on, however I would estimate I spent more than 50 hours in total, possibly as many as 75. 

I feel that this project taught me what it was meant to teach me, and do not have any specific complaints about it. 
If the project were set later in the class, to when we had covered the moments of a distribution, I think it would have been instructive to analyze how the different sample moments converge at different speeds to their population equivalents, however I can see how this would disrupt the schedule of an already finely-tuned class. 


 \newpage
 \subsection*{Graphs}
 \begin{figure}[h]
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-1-fig-1.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-1-fig-2.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-1-fig-3.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-1-fig-4.png}
     \end{subfigure}
     \caption{Graphs for Problem 3.1}
     \label{fig:p1}
 \end{figure}
 
 \begin{figure}[h]
     \centering
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-2-fig-1.png}
         \caption{$p_1 = 0.5$}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-2-fig-2.png}
         \caption{$p_1 = 0.7$}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-2-fig-3.png}
         \caption{$p_1 = 0.2$}
     \end{subfigure}
     \caption{Graphs for Problem 3.2}
     \label{fig:p2}
 \end{figure}
 
 \begin{figure}[h]
     \centering
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-3-fig-1.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-3-fig-2.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-3-fig-3.png}
     \end{subfigure}
     \caption{Graphs for Problem 3.3}
     \label{fig:p3}
 \end{figure}
 
 \newpage
 \begin{figure}[h]
     \centering
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-4-fig-1.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-4-fig-2.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-4-fig-3.png}
     \end{subfigure}
     \caption{Graphs for Problem 3.4}
     \label{fig:p4}
 \end{figure}
 
 \newpage
 \begin{figure}[h]
     \centering
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-5-fig-1.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-5-fig-2.png}
     \end{subfigure}
     \hfill
     \begin{subfigure}{0.55\textwidth}
         \includegraphics[width=\textwidth]{problem-3-5-fig-3.png}
     \end{subfigure}
     \caption{Graphs for Problem 3.5}
     \label{fig:p5}
 \end{figure}

 
\end{document}
